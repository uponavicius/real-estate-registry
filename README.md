# real-estate-registry

With this API can CRUD owners and buildings. By tax rates can calculate total yearly real estate tax for all properties owned by a particular owner.

Back-End REST API was created using Spring Boot framework. Back-End working with H2 in memory database.

### To start project:
* Clone repository https://gitlab.com/uponavicius/real-estate-registry.git
* Open with your favorite IDEA (example IntelliJ IDEA)
* Run project
* Open project documentation http://localhost:8080/swagger-ui.html 
* You can create demo data http://localhost:8080/swagger-ui.html#/owner-controller/createDemoDataUsingGET
* Try all other methods.
* Run all Junit tests from `src\test\java\com\upondev\realestateregistry\service\impl`

#### Tests with postman
* Open postman and import `(menu File/Import.../Upload Files)` file `Real Estate Registry.postman_collection.json` from `src\main\resources\postman`

#### REST-assured tests `real-estate-registry-rest-assured-tests`
* How to run real-estate-registry-rest-assured-tests find in here - https://gitlab.com/uponavicius/real-estate-registry-rest-assured-tests
