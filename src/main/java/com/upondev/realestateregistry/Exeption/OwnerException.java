package com.upondev.realestateregistry.Exeption;

public class OwnerException extends RuntimeException {
    private static final long serialVersionUID = 2792092795164512039L;

    public OwnerException(String message) {
        super(message);
    }
}
