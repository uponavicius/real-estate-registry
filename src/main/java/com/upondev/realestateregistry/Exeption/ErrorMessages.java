package com.upondev.realestateregistry.Exeption;

public enum ErrorMessages {
    NO_OWNER_FOUND("Owner Not found"),
    NOT_FILED_MANDATORY_OWNER_FIELDS("Please fill first name and last name fields"),
    NOT_FILED_MANDATORY_BUILDING_FIELDS("Please fill property type, city and street fields"),
    SIZE_AND_MARKET_VALUE_GREATER_THAN_ZERO("Building size and market value must to be greater than zero"),
    INCORRECT_PROPERTY_NAME("Incorrect property name"),
    NO_BUILDING_FOUND("Building Not found");

    private String errorMessage;

    ErrorMessages(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}

