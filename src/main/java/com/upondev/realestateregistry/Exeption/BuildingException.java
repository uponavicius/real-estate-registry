package com.upondev.realestateregistry.Exeption;

public class BuildingException extends RuntimeException {
    private static final long serialVersionUID = 1583065547860252448L;

    public BuildingException(String message) {
        super(message);
    }
}
