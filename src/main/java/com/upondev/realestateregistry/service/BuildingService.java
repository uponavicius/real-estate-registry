package com.upondev.realestateregistry.service;

import com.upondev.realestateregistry.model.entity.BuildingEntity;

import java.util.List;

public interface BuildingService {
    BuildingEntity createBuilding(Long ownerId, BuildingEntity building);

    List<BuildingEntity> getBuildings(Long ownerId);

    BuildingEntity getBuilding(Long id);

    BuildingEntity updateBuilding(Long id, BuildingEntity building);

    boolean deleteBuilding(Long id);
}
