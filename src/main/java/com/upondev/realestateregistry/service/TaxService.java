package com.upondev.realestateregistry.service;

public interface TaxService {
    double getOwnerTaxesForAllBuildings(Long ownerId);
}
