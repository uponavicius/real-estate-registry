package com.upondev.realestateregistry.service.impl;

import com.upondev.realestateregistry.model.entity.BuildingEntity;
import com.upondev.realestateregistry.model.entity.OwnerEntity;
import com.upondev.realestateregistry.service.OwnerService;
import com.upondev.realestateregistry.service.TaxService;
import com.upondev.realestateregistry.utils.ServiceUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaxServiceImpl implements TaxService {

    final OwnerService ownerService;
    final ServiceUtils serviceUtils;

    public TaxServiceImpl(OwnerService ownerService, ServiceUtils serviceUtils) {
        this.ownerService = ownerService;
        this.serviceUtils = serviceUtils;
    }

    @Override
    public double getOwnerTaxesForAllBuildings(Long ownerId) {
        OwnerEntity owner = ownerService.getOwner(ownerId);
        List<BuildingEntity> buildings = owner.getBuildings();

        double ownerTaxes = 0;
        for (BuildingEntity building : buildings) {
            ownerTaxes += ((building.getMarketValue() / 100) * serviceUtils
                    .getBuildingTaxRate(building.getPropertyType().toLowerCase()));
        }

        return ownerTaxes;
    }
}
