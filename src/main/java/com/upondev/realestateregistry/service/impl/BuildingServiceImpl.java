package com.upondev.realestateregistry.service.impl;

import com.upondev.realestateregistry.model.entity.BuildingEntity;
import com.upondev.realestateregistry.model.entity.OwnerEntity;
import com.upondev.realestateregistry.model.repository.BuildingRepository;
import com.upondev.realestateregistry.service.BuildingService;
import com.upondev.realestateregistry.utils.ServiceUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BuildingServiceImpl implements BuildingService {

    final ServiceUtils serviceUtils;
    final BuildingRepository buildingRepository;

    public BuildingServiceImpl(BuildingRepository buildingRepository, ServiceUtils serviceUtils) {
        this.buildingRepository = buildingRepository;
        this.serviceUtils = serviceUtils;
    }

    @Override
    public BuildingEntity createBuilding(Long ownerId, BuildingEntity building) {
        serviceUtils.checkBuildingMandatoryFields(building);

        OwnerEntity owner = serviceUtils.getOwnerById(ownerId);
        BuildingEntity buildingToCreate = new BuildingEntity();
        BeanUtils.copyProperties(building, buildingToCreate);

        buildingToCreate.setOwner(owner);

        serviceUtils.findPropertyName(buildingToCreate, building.getPropertyType().toLowerCase());

        return buildingRepository.save(buildingToCreate);
    }

    @Override
    public List<BuildingEntity> getBuildings(Long ownerId) {
        return buildingRepository.findByOwnerId(ownerId);
    }

    @Override
    public BuildingEntity getBuilding(Long buildingId) {
        return serviceUtils.getBuildingById(buildingId);
    }

    @Override
    public BuildingEntity updateBuilding(Long buildingId, BuildingEntity building) {
        serviceUtils.checkBuildingMandatoryFields(building);

        BuildingEntity buildingForUpdate = serviceUtils.getBuildingById(buildingId);
        OwnerEntity owner = serviceUtils.getOwnerById(buildingForUpdate.getOwner().getId());

        BeanUtils.copyProperties(building, buildingForUpdate);
        buildingForUpdate.setId(buildingId);
        buildingForUpdate.setOwner(owner);

        serviceUtils.findPropertyName(buildingForUpdate, building.getPropertyType().toLowerCase());

        return buildingRepository.save(buildingForUpdate);
    }

    @Override
    public boolean deleteBuilding(Long id) {
        if (buildingRepository.existsById(id)) {
            buildingRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
