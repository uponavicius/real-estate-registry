package com.upondev.realestateregistry.service.impl;

import com.upondev.realestateregistry.model.entity.OwnerEntity;
import com.upondev.realestateregistry.model.repository.OwnerRepository;
import com.upondev.realestateregistry.service.OwnerService;
import com.upondev.realestateregistry.utils.ServiceUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OwnerServiceImpl implements OwnerService {

    final ServiceUtils serviceUtils;
    final OwnerRepository ownerRepository;

    public OwnerServiceImpl(OwnerRepository ownerRepository, ServiceUtils serviceUtils) {
        this.ownerRepository = ownerRepository;
        this.serviceUtils = serviceUtils;
    }

    @Override
    public OwnerEntity createOwner(OwnerEntity owner) {
        serviceUtils.checkOwnerMandatoryFields(owner);
        return ownerRepository.save(owner);
    }

    @Override
    public List<OwnerEntity> getOwners() {
        return ownerRepository.findAll();
    }

    @Override
    public OwnerEntity getOwner(Long id) {
        return serviceUtils.getOwnerById(id);
    }

    @Override
    public OwnerEntity updateOwner(Long ownerId, OwnerEntity owner) {
        serviceUtils.checkOwnerMandatoryFields(owner);
        OwnerEntity ownerToUpdate = serviceUtils.getOwnerById(ownerId);

        BeanUtils.copyProperties(owner, ownerToUpdate);
        ownerToUpdate.setId(ownerId);

        return ownerRepository.save(ownerToUpdate);
    }

    @Override
    public boolean deleteOwner(Long id) {
        if (ownerRepository.existsById(id)) {
            ownerRepository.deleteById(id);
            return true;
        }
        return false;
    }

}
