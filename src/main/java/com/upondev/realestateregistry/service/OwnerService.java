package com.upondev.realestateregistry.service;

import com.upondev.realestateregistry.model.entity.OwnerEntity;

import java.util.List;

public interface OwnerService {

    OwnerEntity createOwner(OwnerEntity owner);

    List<OwnerEntity> getOwners();

    OwnerEntity getOwner(Long id);

    OwnerEntity updateOwner(Long id, OwnerEntity owner);

    boolean deleteOwner(Long id);
}
