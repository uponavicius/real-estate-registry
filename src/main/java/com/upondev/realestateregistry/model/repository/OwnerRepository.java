package com.upondev.realestateregistry.model.repository;

import com.upondev.realestateregistry.model.entity.OwnerEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OwnerRepository extends CrudRepository<OwnerEntity, Long> {
    List<OwnerEntity> findAll();

    OwnerEntity findByFirstNameAndLastName(String firstName, String lastName);
}
