package com.upondev.realestateregistry.model.repository;

import com.upondev.realestateregistry.model.entity.BuildingEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BuildingRepository extends CrudRepository<BuildingEntity, Long> {
    List<BuildingEntity> findByOwnerId(Long ownerId);
}
