package com.upondev.realestateregistry.model.response;

public enum RequestOperationStatus {
    ERROR,
    SUCCESS
}
