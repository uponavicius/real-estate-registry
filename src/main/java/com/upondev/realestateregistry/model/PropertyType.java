package com.upondev.realestateregistry.model;

public enum PropertyType {
    APARTMENT("Apartment"),
    HOUSE("House"),
    INDUSTRIAL("Industrial"),
    OFFICE("Office"),
    MULTIFAMILY("Multifamily"),
    RETAIL("Retail");

    private String typeName;

    PropertyType(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
