package com.upondev.realestateregistry.demodata;

import com.upondev.realestateregistry.model.entity.BuildingEntity;
import com.upondev.realestateregistry.model.entity.OwnerEntity;
import com.upondev.realestateregistry.model.repository.OwnerRepository;
import com.upondev.realestateregistry.service.impl.BuildingServiceImpl;
import com.upondev.realestateregistry.service.impl.OwnerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DemoData {

    @Autowired
    OwnerServiceImpl ownerService;

    @Autowired
    OwnerRepository ownerRepository;

    @Autowired
    BuildingServiceImpl buildingService;

    public List<OwnerEntity> createOwners() {
        List<OwnerEntity> demoOwners = new ArrayList<>();

        OwnerEntity ownerVladas = new OwnerEntity();
        ownerVladas.setFirstName("Vladas");
        ownerVladas.setLastName("Uponavicius");
        ownerVladas.setPhone(37060000000L);
        demoOwners.add(ownerService.createOwner(ownerVladas));

        OwnerEntity ownerJohn = new OwnerEntity();
        ownerJohn.setFirstName("John");
        ownerJohn.setLastName("Smith");
        ownerJohn.setPhone(37060000001L);
        demoOwners.add(ownerService.createOwner(ownerJohn));

        OwnerEntity ownerHubert = new OwnerEntity();
        ownerHubert.setFirstName("Hubert");
        ownerHubert.setLastName("Smith");
        ownerHubert.setPhone(37060000002L);
        demoOwners.add(ownerService.createOwner(ownerHubert));

        createBuildings();

        return demoOwners;

    }

    public void createBuildings() {
        OwnerEntity vladasOwner = ownerRepository.findByFirstNameAndLastName("Vladas", "Uponavicius");
        Long vladasId = vladasOwner.getId();

        BuildingEntity vladasApartment = new BuildingEntity();
        vladasApartment.setPropertyType("Apartment");
        vladasApartment.setCity("Vilnius");
        vladasApartment.setStreet("Gedimino");
        vladasApartment.setNumber("25-25");
        vladasApartment.setSize(100.0);
        vladasApartment.setMarketValue(150000.0);
        vladasApartment.setOwner(vladasOwner);
        buildingService.createBuilding(vladasId, vladasApartment);

        BuildingEntity vladasHouse = new BuildingEntity();
        vladasHouse.setPropertyType("House");
        vladasHouse.setCity("Vilnius");
        vladasHouse.setStreet("Sostines");
        vladasHouse.setNumber("321");
        vladasHouse.setSize(180.0);
        vladasHouse.setMarketValue(240000.0);
        vladasApartment.setOwner(vladasOwner);
        buildingService.createBuilding(vladasId, vladasHouse);

        BuildingEntity vladasOffice = new BuildingEntity();
        vladasOffice.setPropertyType("Office");
        vladasOffice.setCity("Vilnius");
        vladasOffice.setStreet("Jasinskio");
        vladasOffice.setNumber("19");
        vladasOffice.setSize(2400.0);
        vladasOffice.setMarketValue(10000000.0);
        vladasApartment.setOwner(vladasOwner);
        buildingService.createBuilding(vladasId, vladasOffice);

        OwnerEntity johnOwner = ownerRepository.findByFirstNameAndLastName("John", "Smith");
        Long johnId = johnOwner.getId();

        BuildingEntity johnRetail = new BuildingEntity();
        johnRetail.setPropertyType("Retail");
        johnRetail.setCity("Vilnius");
        johnRetail.setStreet("Sostines");
        johnRetail.setNumber("321");
        johnRetail.setSize(180.0);
        johnRetail.setMarketValue(170000);
        johnRetail.setOwner(johnOwner);
        buildingService.createBuilding(johnId, johnRetail);

        BuildingEntity johnApartment = new BuildingEntity();
        johnApartment.setPropertyType("Apartment");
        johnApartment.setCity("Vilnius");
        johnApartment.setStreet("Gedimino");
        johnApartment.setNumber("25-25");
        johnApartment.setSize(150.0);
        johnApartment.setMarketValue(240000.0);
        johnRetail.setOwner(johnOwner);
        buildingService.createBuilding(johnId, johnApartment);

        OwnerEntity hubertOwner = ownerRepository.findByFirstNameAndLastName("Hubert", "Smith");
        Long hubertId = hubertOwner.getId();

        BuildingEntity hubertIndustrial = new BuildingEntity();
        hubertIndustrial.setPropertyType("Industrial");
        hubertIndustrial.setCity("Vilnius");
        hubertIndustrial.setStreet("Sostines");
        hubertIndustrial.setNumber("321");
        hubertIndustrial.setSize(15000);
        hubertIndustrial.setMarketValue(1700000);
        hubertIndustrial.setOwner(hubertOwner);
        buildingService.createBuilding(hubertId, hubertIndustrial);

        BuildingEntity hubertMultifamily = new BuildingEntity();
        hubertMultifamily.setPropertyType("Multifamily");
        hubertMultifamily.setCity("Vilnius");
        hubertMultifamily.setStreet("Gedimino");
        hubertMultifamily.setNumber("25-25");
        hubertMultifamily.setSize(1500.0);
        hubertMultifamily.setMarketValue(1200000.0);
        hubertIndustrial.setOwner(hubertOwner);
        buildingService.createBuilding(hubertId, hubertMultifamily);

    }


}
