package com.upondev.realestateregistry.controller;

import com.upondev.realestateregistry.demodata.DemoData;
import com.upondev.realestateregistry.model.entity.OwnerEntity;
import com.upondev.realestateregistry.model.response.OperationStatusModel;
import com.upondev.realestateregistry.model.response.RequestOperationName;
import com.upondev.realestateregistry.model.response.RequestOperationStatus;
import com.upondev.realestateregistry.service.impl.OwnerServiceImpl;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("owner")
public class OwnerController {

    final OwnerServiceImpl ownerService;
    final DemoData demoData;

    public OwnerController(OwnerServiceImpl ownerService, DemoData demoData) {
        this.ownerService = ownerService;
        this.demoData = demoData;
    }

    @ApiOperation(value = "Create new owner.", notes = "${ownerController.CreateOwner.ApiOperation.Notes}")
    @PostMapping()
    public OwnerEntity createOwner(@RequestBody OwnerEntity owner) {
        return ownerService.createOwner(owner);
    }

    @ApiOperation(value = "Get all owners list.", notes = "${ownerController.GetOwners.ApiOperation.Notes}")
    @GetMapping()
    public List<OwnerEntity> getOwners() {
        return ownerService.getOwners();
    }

    @ApiOperation(value = "Get owner by id.", notes = "${ownerController.GetOwner.ApiOperation.Notes}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "Owner id")
    })
    @GetMapping(path = "/{id}")
    public OwnerEntity getOwner(@PathVariable Long id) {
        return ownerService.getOwner(id);
    }

    @ApiOperation(value = "Update owner details.", notes = "${ownerController.UpdateOwner.ApiOperation.Notes}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "Owner id")
    })
    @PutMapping(path = "/{id}")
    public OwnerEntity updateOwner(@RequestBody OwnerEntity owner, @PathVariable Long id) {
        return ownerService.updateOwner(id, owner);
    }

    @ApiOperation(value = "Delete owner with all buildings.", notes = "${ownerController.DeleteOwner.ApiOperation.Notes}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "Owner id")
    })
    @DeleteMapping(path = "/{id}")
    public OperationStatusModel deleteOwner(@PathVariable Long id) {
        OperationStatusModel operationStatus = new OperationStatusModel();
        operationStatus.setOperationName(RequestOperationName.DELETE.name());

        if (ownerService.deleteOwner(id)) {
            operationStatus.setOperationResult(RequestOperationStatus.SUCCESS.name());
        } else {
            operationStatus.setOperationResult(RequestOperationStatus.ERROR.name());
        }

        return operationStatus;
    }

    @ApiOperation(value = "Create demo data", notes = "${ownerController.CreateDemoData.ApiOperation.Notes}")
    @GetMapping(path = "/demoData")
    public List<OwnerEntity> createDemoData() {
        return demoData.createOwners();
    }

}
