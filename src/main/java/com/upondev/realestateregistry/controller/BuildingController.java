package com.upondev.realestateregistry.controller;

import com.upondev.realestateregistry.model.entity.BuildingEntity;
import com.upondev.realestateregistry.model.response.OperationStatusModel;
import com.upondev.realestateregistry.model.response.RequestOperationName;
import com.upondev.realestateregistry.model.response.RequestOperationStatus;
import com.upondev.realestateregistry.service.impl.BuildingServiceImpl;
import com.upondev.realestateregistry.service.impl.TaxServiceImpl;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("owner/{ownerId}/building")
public class BuildingController {

    final BuildingServiceImpl buildingService;
    final TaxServiceImpl taxService;

    public BuildingController(BuildingServiceImpl buildingService, TaxServiceImpl taxService) {
        this.buildingService = buildingService;
        this.taxService = taxService;
    }

    @ApiOperation(value = "Create new building for existing owner.", notes = "${buildingController.CreateBuilding.ApiOperation.Notes}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ownerId", value = "Owner id")
    })
    @PostMapping
    public BuildingEntity createBuilding(@PathVariable Long ownerId, @RequestBody BuildingEntity building) {
        return buildingService.createBuilding(ownerId, building);
    }

    @ApiOperation(value = "Get all owner buildings list.", notes = "${buildingController.GetBuildings.ApiOperation.Notes}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ownerId", value = "Owner id")
    })
    @GetMapping()
    public List<BuildingEntity> getOwnerBuildings(@PathVariable Long ownerId) {
        return buildingService.getBuildings(ownerId);
    }

    @ApiOperation(value = "Get one owner building.", notes = "${buildingController.GetBuilding.ApiOperation.Notes}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ownerId", value = "Owner id"),
            @ApiImplicitParam(name = "buildingId", value = "Building Id")
    })
    @GetMapping(path = "/{buildingId}")
    public BuildingEntity getBuilding(@PathVariable Long buildingId) {
        return buildingService.getBuilding(buildingId);
    }

    @ApiOperation(value = "Update owner building details.", notes = "${buildingController.UpdateBuilding.ApiOperation.Notes}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ownerId", value = "Owner id"),
            @ApiImplicitParam(name = "buildingId", value = "Building Id")
    })
    @PutMapping(path = "/{buildingId}")
    public BuildingEntity updateBuilding(@PathVariable Long buildingId, @RequestBody BuildingEntity building) {
        return buildingService.updateBuilding(buildingId, building);
    }

    @ApiOperation(value = "Delete owner buildings.", notes = "${buildingController.DeleteBuilding.ApiOperation.Notes}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ownerId", value = "Owner id"),
            @ApiImplicitParam(name = "buildingId", value = "Building Id")
    })
    @DeleteMapping(path = "/{buildingId}")
    public OperationStatusModel deleteBuilding(@PathVariable Long buildingId) {
        OperationStatusModel operationStatus = new OperationStatusModel();
        operationStatus.setOperationName(RequestOperationName.DELETE.name());

        if (buildingService.deleteBuilding(buildingId)) {
            operationStatus.setOperationResult(RequestOperationStatus.SUCCESS.name());
        } else {
            operationStatus.setOperationResult(RequestOperationStatus.ERROR.name());
        }
        return operationStatus;
    }

    @ApiOperation(value = "Get owner yearly taxes for all properties.", notes = "${buildingController.GetTaxes.ApiOperation.Notes}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ownerId", value = "Owner id")
    })
    @GetMapping(path = "/taxes")
    public double getTaxes(@PathVariable Long ownerId) {
        return taxService.getOwnerTaxesForAllBuildings(ownerId);
    }


}
