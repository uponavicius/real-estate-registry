package com.upondev.realestateregistry;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class AppProperties {

    private final Environment env;

    public AppProperties(Environment env) {
        this.env = env;
    }

    public double getApartmentTaxRate() {
        return Double.parseDouble(Objects.requireNonNull(env.getProperty("apartmentTaxRate")));
    }

    public double getHouseTaxRate() {
        return Double.parseDouble(Objects.requireNonNull(env.getProperty("houseTaxRate")));
    }

    public double getIndustrialTaxRate() {
        return Double.parseDouble(Objects.requireNonNull(env.getProperty("industrialTaxRate")));
    }

    public double getOfficeTaxRate() {
        return Double.parseDouble(Objects.requireNonNull(env.getProperty("officeTaxRate")));
    }

    public double getMultifamilyTaxRate() {
        return Double.parseDouble(Objects.requireNonNull(env.getProperty("multifamilyTaxRate")));
    }

    public double getRetailTaxRate() {
        return Double.parseDouble(Objects.requireNonNull(env.getProperty("retailTaxRate")));
    }


}
