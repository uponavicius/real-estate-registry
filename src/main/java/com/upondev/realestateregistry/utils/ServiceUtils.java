package com.upondev.realestateregistry.utils;

import com.upondev.realestateregistry.AppProperties;
import com.upondev.realestateregistry.Exeption.BuildingException;
import com.upondev.realestateregistry.Exeption.ErrorMessages;
import com.upondev.realestateregistry.Exeption.OwnerException;
import com.upondev.realestateregistry.model.PropertyType;
import com.upondev.realestateregistry.model.entity.BuildingEntity;
import com.upondev.realestateregistry.model.entity.OwnerEntity;
import com.upondev.realestateregistry.model.repository.BuildingRepository;
import com.upondev.realestateregistry.model.repository.OwnerRepository;
import org.springframework.stereotype.Component;

@Component
public class ServiceUtils {

    final OwnerRepository ownerRepository;
    final BuildingRepository buildingRepository;
    final AppProperties appProperties;

    public ServiceUtils(OwnerRepository ownerRepository, BuildingRepository buildingRepository, AppProperties appProperties) {
        this.ownerRepository = ownerRepository;
        this.buildingRepository = buildingRepository;
        this.appProperties = appProperties;
    }

    public OwnerEntity getOwnerById(Long ownerId) {
        return ownerRepository.findById(ownerId)
                .orElseThrow(() -> new OwnerException(ErrorMessages.NO_OWNER_FOUND.getErrorMessage()));
    }

    public void checkOwnerMandatoryFields(OwnerEntity owner) {
        if (owner.getFirstName().isEmpty() || owner.getLastName().isEmpty())
            throw new OwnerException(ErrorMessages.NOT_FILED_MANDATORY_OWNER_FIELDS.getErrorMessage());
    }

    public BuildingEntity getBuildingById(Long buildingId) {
        return buildingRepository.findById(buildingId)
                .orElseThrow(() -> new BuildingException(ErrorMessages.NO_BUILDING_FOUND.getErrorMessage()));
    }

    public void checkBuildingMandatoryFields(BuildingEntity building) {
        if (building.getPropertyType().isEmpty() || building.getCity().isEmpty() || building.getStreet().isEmpty())
            throw new BuildingException(ErrorMessages.NOT_FILED_MANDATORY_BUILDING_FIELDS.getErrorMessage());

        if (building.getSize() <= 0 || building.getMarketValue() <= 0)
            throw new BuildingException(ErrorMessages.SIZE_AND_MARKET_VALUE_GREATER_THAN_ZERO.getErrorMessage());
    }

    public void findPropertyName(BuildingEntity buildingToCreate, String propertyType) {
        switch (propertyType) {
            case "apartment":
                buildingToCreate.setPropertyType(PropertyType.APARTMENT.getTypeName());
                break;
            case "house":
                buildingToCreate.setPropertyType(PropertyType.HOUSE.getTypeName());
                break;
            case "industrial":
                buildingToCreate.setPropertyType(PropertyType.INDUSTRIAL.getTypeName());
                break;
            case "office":
                buildingToCreate.setPropertyType(PropertyType.OFFICE.getTypeName());
                break;
            case "multifamily":
                buildingToCreate.setPropertyType(PropertyType.MULTIFAMILY.getTypeName());
                break;
            case "retail":
                buildingToCreate.setPropertyType(PropertyType.RETAIL.getTypeName());
                break;
            default:
                throw new BuildingException(ErrorMessages.INCORRECT_PROPERTY_NAME.getErrorMessage());
        }
    }

    public double getBuildingTaxRate(String propertyType) {
        switch (propertyType) {
            case "apartment":
                return appProperties.getApartmentTaxRate();
            case "house":
                return appProperties.getHouseTaxRate();
            case "industrial":
                return appProperties.getIndustrialTaxRate();
            case "office":
                return appProperties.getOfficeTaxRate();
            case "multifamily":
                return appProperties.getMultifamilyTaxRate();
            case "retail":
                return appProperties.getRetailTaxRate();
            default:
                throw new BuildingException(ErrorMessages.INCORRECT_PROPERTY_NAME.getErrorMessage());
        }
    }

}
