package com.upondev.realestateregistry.service.impl;

import com.upondev.realestateregistry.model.entity.BuildingEntity;
import com.upondev.realestateregistry.model.entity.OwnerEntity;
import com.upondev.realestateregistry.model.repository.OwnerRepository;
import com.upondev.realestateregistry.utils.ServiceUtils;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class OwnerServiceImplTest {

    OwnerEntity ownerEntity;
    List<OwnerEntity> ownerEntities = new ArrayList<>();

    @InjectMocks
    OwnerServiceImpl ownerService;

    @Mock
    OwnerRepository ownerRepository;

    @Mock
    ServiceUtils serviceUtils;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        createOwnerData();
        createOwners();
    }

    @Test
    void createOwner() {
        doNothing().when(serviceUtils).checkOwnerMandatoryFields(ownerEntity);

        ownerEntity.setId(1L);
        when(ownerRepository.save(any(OwnerEntity.class))).thenReturn(ownerEntity);
        OwnerEntity createdOwner = ownerService.createOwner(ownerEntity);

        assertNotNull(createdOwner);
        assertEquals(ownerEntity.getId(), createdOwner.getId());
        assertEquals(ownerEntity.getFirstName(), createdOwner.getFirstName());
        assertEquals(ownerEntity.getLastName(), createdOwner.getLastName());
        assertEquals(ownerEntity.getPhone(), createdOwner.getPhone());
        assertEquals(3, createdOwner.getBuildings().size());

        checkBuildingsValues(createdOwner);
        verify(ownerRepository, times(1)).save(any(OwnerEntity.class));
    }

    @Test
    void getOwners() {
        when(ownerRepository.findAll()).thenReturn(ownerEntities);
        List<OwnerEntity> storedOwners = ownerService.getOwners();
        assertNotNull(storedOwners);
        verify(ownerRepository, times(1)).findAll();
        assertEquals(2, storedOwners.size());

        for (int i = 0; i < storedOwners.size(); i++) {
            assertEquals(ownerEntities.get(i).getId(), storedOwners.get(i).getId());
            assertEquals(ownerEntities.get(i).getFirstName(), storedOwners.get(i).getFirstName());
            assertEquals(ownerEntities.get(i).getLastName(), storedOwners.get(i).getLastName());
            assertEquals(ownerEntities.get(i).getPhone(), storedOwners.get(i).getPhone());
        }

    }

    @Test
    void getOwner() {
        ownerEntity.setId(1L);

        when(serviceUtils.getOwnerById(anyLong())).thenReturn(ownerEntity);
        OwnerEntity returnedEntity = ownerService.getOwner(1L);
        verify(serviceUtils, times(1)).getOwnerById(anyLong());

        assertNotNull(returnedEntity);
        assertEquals(1L, returnedEntity.getId());
        assertEquals("Vladas", returnedEntity.getFirstName());
        assertEquals("Uponavicius", returnedEntity.getLastName());
        assertEquals(37060000000L, returnedEntity.getPhone());
        assertEquals(3, returnedEntity.getBuildings().size());

        checkBuildingsValues(returnedEntity);
    }

    @Test
    void updateOwner() {
        ownerEntity.setId(1L);
        when(serviceUtils.getOwnerById(anyLong())).thenReturn(ownerEntity);
        OwnerEntity ownerToUpdate = serviceUtils.getOwnerById(1L);
        assertNotNull(ownerToUpdate);

        OwnerEntity dataForUpdate = new OwnerEntity();
        dataForUpdate.setFirstName("Hubert");
        dataForUpdate.setLastName("Smith");
        dataForUpdate.setPhone(37060000011L);

        BeanUtils.copyProperties(ownerToUpdate, dataForUpdate);
        ownerToUpdate.setId(1L);

        when(ownerRepository.save(any(OwnerEntity.class))).thenReturn(dataForUpdate);
        OwnerEntity updatedOwner = ownerService.updateOwner(1l, ownerToUpdate);

        assertNotNull(updatedOwner);

        assertEquals(ownerToUpdate.getId(), updatedOwner.getId());
        assertEquals(ownerToUpdate.getFirstName(), updatedOwner.getFirstName());
        assertEquals(ownerToUpdate.getLastName(), updatedOwner.getLastName());
        assertEquals(ownerToUpdate.getPhone(), updatedOwner.getPhone());
        assertEquals(ownerToUpdate.getBuildings().size(), updatedOwner.getBuildings().size());
    }

    @Test
    void deleteOwner() {
        when(ownerRepository.existsById(anyLong())).thenReturn(true);

        doNothing().when(ownerRepository).deleteById(anyLong());

        Boolean isDeletedOwner = ownerService.deleteOwner(1L);
        assertEquals(true, isDeletedOwner);
    }

    private void createOwnerData() {
        ownerEntity = new OwnerEntity();
        ownerEntity.setFirstName("Vladas");
        ownerEntity.setLastName("Uponavicius");
        ownerEntity.setPhone(37060000000L);

        BuildingEntity apartment = new BuildingEntity();
        apartment.setPropertyType("Apartment");
        apartment.setCity("Vilnius");
        apartment.setStreet("Gedimino");
        apartment.setNumber("25-25");
        apartment.setSize(100.0);
        apartment.setMarketValue(100000.0);
        apartment.setOwner(ownerEntity);

        BuildingEntity house = new BuildingEntity();
        house.setPropertyType("House");
        house.setCity("Vilnius");
        house.setStreet("Sostines");
        house.setNumber("321");
        house.setSize(180.0);
        house.setMarketValue(100000.0);

        BuildingEntity office = new BuildingEntity();
        office.setPropertyType("Office");
        office.setCity("Vilnius");
        office.setStreet("Jasinskio");
        office.setNumber("19");
        office.setSize(2400.0);
        office.setMarketValue(100000.0);

        List<BuildingEntity> buildings = new ArrayList<>();
        buildings.add(apartment);
        buildings.add(house);
        buildings.add(office);

        ownerEntity.setBuildings(buildings);
    }

    private void createOwners() {
        OwnerEntity ownerJohn = new OwnerEntity();
        ownerJohn.setId(1L);
        ownerJohn.setFirstName("John");
        ownerJohn.setLastName("Smith");
        ownerJohn.setPhone(37060000000L);

        OwnerEntity ownerHubert = new OwnerEntity();
        ownerHubert.setId(1L);
        ownerHubert.setFirstName("John");
        ownerHubert.setLastName("Smith");
        ownerHubert.setPhone(37060000000L);

        ownerEntities.add(ownerJohn);
        ownerEntities.add(ownerHubert);


    }

    private void checkBuildingsValues(OwnerEntity storeEntity) {
        List<BuildingEntity> buildings = storeEntity.getBuildings();

        for (int i = 0; i < buildings.size(); i++) {
            assertEquals(ownerEntity.getBuildings().get(i).getPropertyType(),
                    storeEntity.getBuildings().get(i).getPropertyType()
            );

            assertEquals(
                    ownerEntity.getBuildings().get(i).getMarketValue(),
                    storeEntity.getBuildings().get(i).getMarketValue()
            );

            assertEquals(
                    ownerEntity.getBuildings().get(i).getCity(),
                    storeEntity.getBuildings().get(i).getCity()
            );

            assertEquals(
                    ownerEntity.getBuildings().get(i).getOwner(),
                    storeEntity.getBuildings().get(i).getOwner()
            );

            assertEquals(
                    ownerEntity.getBuildings().get(i).getSize(),
                    storeEntity.getBuildings().get(i).getSize()
            );

            assertEquals(
                    ownerEntity.getBuildings().get(i).getStreet(),
                    storeEntity.getBuildings().get(i).getStreet()
            );

            assertEquals(
                    ownerEntity.getBuildings().get(i).getId(),
                    storeEntity.getBuildings().get(i).getId()
            );

            assertEquals(
                    ownerEntity.getBuildings().get(i).getNumber(),
                    storeEntity.getBuildings().get(i).getNumber()
            );
        }
    }

}
