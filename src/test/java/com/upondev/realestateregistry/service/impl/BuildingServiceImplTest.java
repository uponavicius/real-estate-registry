package com.upondev.realestateregistry.service.impl;

import com.upondev.realestateregistry.model.entity.BuildingEntity;
import com.upondev.realestateregistry.model.entity.OwnerEntity;
import com.upondev.realestateregistry.model.repository.BuildingRepository;
import com.upondev.realestateregistry.utils.ServiceUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

class BuildingServiceImplTest {

    OwnerEntity owner;
    BuildingEntity building;
    List<BuildingEntity> buildings = new ArrayList<>();

    @InjectMocks
    BuildingServiceImpl buildingService;

    @Mock
    ServiceUtils serviceUtils;

    @Mock
    BuildingRepository buildingRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        creteOwner();
        createNewBuilding();
        buildings.add(building);
        buildings.add(building);
        buildings.add(building);
    }

    @Test
    void createBuilding() {
        BuildingEntity buildingToReturn = building;
        buildingToReturn.setOwner(owner);

        when(buildingRepository.save(any(BuildingEntity.class))).thenReturn(buildingToReturn);
        BuildingEntity createdBuilding = buildingService.createBuilding(1L, building);
        assertNotNull(createdBuilding);
        verify(buildingRepository, times(1)).save(any(BuildingEntity.class));

        assertEquals(buildingToReturn.getId(), createdBuilding.getId());
        assertEquals(buildingToReturn.getNumber(), createdBuilding.getNumber());
        assertEquals(buildingToReturn.getStreet(), createdBuilding.getStreet());
        assertEquals(buildingToReturn.getSize(), createdBuilding.getSize());
        assertEquals(buildingToReturn.getOwner(), createdBuilding.getOwner());
        assertEquals(buildingToReturn.getCity(), createdBuilding.getCity());
        assertEquals(buildingToReturn.getMarketValue(), createdBuilding.getMarketValue());
        assertEquals(buildingToReturn.getPropertyType(), createdBuilding.getPropertyType());
    }

    @Test
    void getBuildings() {
        when(buildingRepository.findByOwnerId(anyLong())).thenReturn(buildings);
        List<BuildingEntity> ownerBuildings = buildingRepository.findByOwnerId(1L);
        assertNotNull(ownerBuildings);
        verify(buildingRepository, times(1)).findByOwnerId(anyLong());
        assertEquals(buildings.size(), ownerBuildings.size());

        for (int i = 0; i < ownerBuildings.size(); i++) {
            assertEquals(buildings.get(i).getId(), ownerBuildings.get(i).getId());
            assertEquals(buildings.get(i).getNumber(), ownerBuildings.get(i).getNumber());
            assertEquals(buildings.get(i).getStreet(), ownerBuildings.get(i).getStreet());
            assertEquals(buildings.get(i).getSize(), ownerBuildings.get(i).getSize());
            assertEquals(buildings.get(i).getOwner(), ownerBuildings.get(i).getOwner());
            assertEquals(buildings.get(i).getCity(), ownerBuildings.get(i).getCity());
            assertEquals(buildings.get(i).getMarketValue(), ownerBuildings.get(i).getMarketValue());
            assertEquals(buildings.get(i).getPropertyType(), ownerBuildings.get(i).getPropertyType());
        }

    }

    @Test
    void getBuilding() {
        when(serviceUtils.getBuildingById(anyLong())).thenReturn(building);
        BuildingEntity ownerBuilding = serviceUtils.getBuildingById(1L);
        assertNotNull(ownerBuilding);
        verify(serviceUtils, times(1)).getBuildingById(anyLong());
        assertEquals(building.getPropertyType(), ownerBuilding.getPropertyType());
        assertEquals(building.getMarketValue(), ownerBuilding.getMarketValue());
        assertEquals(building.getCity(), ownerBuilding.getCity());
        assertEquals(building.getOwner(), ownerBuilding.getOwner());
        assertEquals(building.getSize(), ownerBuilding.getSize());
        assertEquals(building.getStreet(), ownerBuilding.getStreet());
        assertEquals(building.getNumber(), ownerBuilding.getNumber());
        assertEquals(building.getId(), ownerBuilding.getId());
    }

    @Test
    void updateBuilding() {
        when(serviceUtils.getBuildingById(anyLong())).thenReturn(building);
        BuildingEntity buildingForUpdate = serviceUtils.getBuildingById(1L);
        assertNotNull(buildingForUpdate);
        assertEquals(building, buildingForUpdate);
        verify(serviceUtils, times(1)).getBuildingById(anyLong());

        when(serviceUtils.getOwnerById(anyLong())).thenReturn(owner);
        OwnerEntity ownerEntity = serviceUtils.getOwnerById(1L);
        assertNotNull(ownerEntity);
        assertEquals(owner, ownerEntity);
        verify(serviceUtils, times(1)).getOwnerById(anyLong());

        BuildingEntity newBuildingData = new BuildingEntity();
        building.setMarketValue(10000);
        newBuildingData.setSize(150);
        newBuildingData.setNumber("20-25");
        newBuildingData.setStreet("Gedimino");
        newBuildingData.setCity("Vilnius");
        newBuildingData.setPropertyType("Apartment");

        BeanUtils.copyProperties(newBuildingData, buildingForUpdate);
        buildingForUpdate.setId(1L);
        buildingForUpdate.setOwner(owner);

        serviceUtils.findPropertyName(buildingForUpdate, newBuildingData.getPropertyType());

        when(buildingRepository.save(any(BuildingEntity.class))).thenReturn(buildingForUpdate);
        BuildingEntity updatedBuilding = buildingRepository.save(buildingForUpdate);
        assertNotNull(updatedBuilding);
        verify(buildingRepository, times(1)).save(any(BuildingEntity.class));

        assertEquals(buildingForUpdate.getId(), updatedBuilding.getId());
        assertEquals(buildingForUpdate.getNumber(), updatedBuilding.getNumber());
        assertEquals(buildingForUpdate.getStreet(), updatedBuilding.getStreet());
        assertEquals(buildingForUpdate.getSize(), updatedBuilding.getSize());
        assertEquals(buildingForUpdate.getOwner(), updatedBuilding.getOwner());
        assertEquals(buildingForUpdate.getCity(), updatedBuilding.getCity());
        assertEquals(buildingForUpdate.getMarketValue(), updatedBuilding.getMarketValue());
        assertEquals(buildingForUpdate.getPropertyType(), updatedBuilding.getPropertyType());


    }

    @Test
    void deleteBuilding() {
        when(buildingRepository.existsById(anyLong())).thenReturn(true);

        doNothing().when(buildingRepository).deleteById(anyLong());

        Boolean isDeletedOwner = buildingService.deleteBuilding(1L);
        assertEquals(true, isDeletedOwner);
    }

    private void creteOwner() {
        owner = new OwnerEntity();
        owner.setId(1L);
        owner.setFirstName("Vladas");
        owner.setLastName("Uponavicius");
        owner.setPhone(37060000000L);
    }

    private void createNewBuilding() {
        building = new BuildingEntity();
        building.setOwner(owner);
        building.setMarketValue(10000);
        building.setSize(100);
        building.setNumber("20-20");
        building.setStreet("Gedimino");
        building.setCity("Vilnius");
        building.setPropertyType("Apartment");
    }
}
