package com.upondev.realestateregistry.service.impl;

import com.upondev.realestateregistry.model.entity.BuildingEntity;
import com.upondev.realestateregistry.model.entity.OwnerEntity;
import com.upondev.realestateregistry.service.OwnerService;
import com.upondev.realestateregistry.utils.ServiceUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

class TaxServiceImplTest {
    OwnerEntity owner;

    @InjectMocks
    TaxServiceImpl taxService;

    @Mock
    OwnerService ownerService;

    @Mock
    ServiceUtils serviceUtils;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        createOwner();
        owner.setBuildings(createBuildings(owner));
    }

    @Test
    void getOwnerTaxesForAllBuildings() {

        when(ownerService.getOwner(anyLong())).thenReturn(owner);
        OwnerEntity ownerEntity = ownerService.getOwner(1L);

        when(serviceUtils.getBuildingTaxRate(anyString())).thenReturn(1.0);

        double taxes = taxService.getOwnerTaxesForAllBuildings(ownerEntity.getId());
        assertEquals(134600, taxes);
    }

    private void createOwner() {
        owner = new OwnerEntity();
        owner.setId(1L);
        owner.setFirstName("Vladas");
        owner.setLastName("Uponavicius");
        owner.setPhone(37060000000L);
    }

    private List<BuildingEntity> createBuildings(OwnerEntity owner) {
        List<BuildingEntity> ownerBuildings = new ArrayList<>();

        BuildingEntity apartment = new BuildingEntity();
        apartment.setPropertyType("Apartment");
        apartment.setCity("Vilnius");
        apartment.setStreet("Gedimino");
        apartment.setNumber("25-25");
        apartment.setSize(100.0);
        apartment.setMarketValue(150000.0);
        apartment.setOwner(owner);

        BuildingEntity house = new BuildingEntity();
        house.setPropertyType("House");
        house.setCity("Vilnius");
        house.setStreet("Sostines");
        house.setNumber("321");
        house.setSize(180.0);
        house.setMarketValue(240000.0);
        apartment.setOwner(owner);

        BuildingEntity office = new BuildingEntity();
        office.setPropertyType("Office");
        office.setCity("Vilnius");
        office.setStreet("Jasinskio");
        office.setNumber("19");
        office.setSize(2400.0);
        office.setMarketValue(10000000.0);
        apartment.setOwner(owner);

        BuildingEntity retail = new BuildingEntity();
        retail.setPropertyType("Retail");
        retail.setCity("Vilnius");
        retail.setStreet("Sostines");
        retail.setNumber("321");
        retail.setSize(180.0);
        retail.setMarketValue(170000);
        retail.setOwner(owner);

        BuildingEntity industrial = new BuildingEntity();
        industrial.setPropertyType("Industrial");
        industrial.setCity("Vilnius");
        industrial.setStreet("Sostines");
        industrial.setNumber("321");
        industrial.setSize(15000);
        industrial.setMarketValue(1700000);
        industrial.setOwner(owner);

        BuildingEntity multifamily = new BuildingEntity();
        multifamily.setPropertyType("Multifamily");
        multifamily.setCity("Vilnius");
        multifamily.setStreet("Gedimino");
        multifamily.setNumber("25-25");
        multifamily.setSize(1500.0);
        multifamily.setMarketValue(1200000.0);
        industrial.setOwner(owner);

        ownerBuildings.add(apartment);
        ownerBuildings.add(house);
        ownerBuildings.add(office);
        ownerBuildings.add(retail);
        ownerBuildings.add(industrial);
        ownerBuildings.add(multifamily);

        return ownerBuildings;
    }


}
